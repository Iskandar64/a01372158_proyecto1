#include <vector>
#include <iostream>
#include "vec3.h"
#include "ray.h"
#include "image_writer.h"
#include "sphere.h"
#include "hitable_list.h"
#include "float.h"
#include "camera.h"

#include <random>
#include "material.h"

float hit_sphere(const vec3& center, float radius, const ray& r)
{
	vec3 oc = r.origin() - center;
	float a = dot(r.direction(), r.direction());
	float b = 2.0 * dot(oc, r.direction());
	float c = dot(oc, oc) - radius * radius;
	float discriminant = b * b - 4 * a*c;
	if (discriminant < 0)
	{
		return -1.0;
	}
	else
	{
		return (-b - sqrt(discriminant)) / (2.0*a);
	}
}

hitable *random_scene() {
	int n = 635;
	hitable **list = new hitable*[n + 1];
	list[0] = new sphere(vec3(0, -1000, 0), 1000, new lambertian(vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -10; a < 15; a++) {
		for (int b = -10; b < 15; b++) {
			float choose_mat = ((double)rand() / (RAND_MAX));
			vec3 center(a + 0.9*((double)rand() / (RAND_MAX)), 0.2, b + 0.9*((double)rand() / (RAND_MAX)));
			if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
				if (choose_mat < 0.8) {  
					list[i++] = new sphere(center, 0.2, new lambertian(vec3(((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)), ((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)), ((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)))));
				}
				else if (choose_mat < 0.95) { 
					list[i++] = new sphere(center, 0.2,
						new metal(vec3(0.5*(1 + ((double)rand() / (RAND_MAX))), 0.5*(1 + ((double)rand() / (RAND_MAX))), 0.5*(1 + ((double)rand() / (RAND_MAX)))), 0.5*((double)rand() / (RAND_MAX))));
				}
				else {  
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}

	list[i++] = new sphere(vec3(-1, 1, 0), 1.0, new metal(vec3(0.3, 0.8, 0.5), 0.7));
	list[i++] = new sphere(vec3(-5, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[i++] = new sphere(vec3(3, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));
	list[i++] = new sphere(vec3(7, 1, 0), 1.0, new lambertian(vec3(0.6, 0.2, 0.7)));

	return new hitable_list(list, i);
}

vec3 color(const ray& r, hitable *world, int depth)
{
	//records al the hits
	hit_record rec;
	if (world->hit(r, 0.001, 2000, rec))
	{
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)) {
			return attenuation * color(scattered, world, depth + 1);
		}
		else
		{
			return vec3(0, 0, 0);
		}
	}
	else
	{
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t) * vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
	}
	
}

int main()
{
	int nx = 1200;
	int ny = 800;
	int ns = 100;
	
	std::vector<unsigned char> pixels;
	hitable *list[4];
	float R = cos(M_PI / 4);
	hitable *world = new hitable_list(list, 4);
	world = random_scene();

	vec3 lookfrom(13, 2, 3);
	vec3 lookat(0, 0, 0);
	float dist_to_focus = 10.0;
	float aperture = 0.1;
	camera cam(lookfrom, lookat, vec3(0, 1, 0), 20, float(nx)/float(ny), aperture, dist_to_focus);

	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float drand48 = ((double)rand() / (RAND_MAX));
				float u = float(i + drand48) / float(nx);
				drand48 = ((double)rand() / (RAND_MAX));
				float v = float(j + drand48) / float(ny);
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color(r, world, 0);
			}			
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99 * col[0]);
			int ig = int(255.99 * col[1]);
			int ib = int(255.99 * col[2]);

			//std::cout << ir << " " << ig << " " << ib << "\n";
			
			pixels.push_back(ir);
			pixels.push_back(ig);
			pixels.push_back(ib);

		}
	}

	image_writer::save_png("FinalEsferaProyecto1.png", nx, ny, 3, pixels.data());

	return 0;
}
